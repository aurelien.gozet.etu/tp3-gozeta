package fr.univlille.iutinfo;

import java.util.HashMap;

public class NumeralUtils {

    public static HashMap<String, String> unite;
    public static HashMap<String, String> decade;
    public static HashMap<String, String> hundred;

    public static HashMap<String, String> decadeParticularity;
    public static HashMap<String, String> normalizer;

    public static HashMap<String, String>[] priority;

    static {
        unite = new HashMap<>();
        unite.put("0", "Zéro");
        unite.put("1", "Un");
        unite.put("2", "Deux");
        unite.put("3", "Trois");
        unite.put("4", "Quatre");
        unite.put("5", "Cinq");
        unite.put("6", "Six");
        unite.put("7", "Sept");
        unite.put("8", "Huit");
        unite.put("9", "Neuf");
        decade = new HashMap<>();
        decade.put("0", "Zéro");
        decade.put("1", "Dix");
        decade.put("2", "Vingt");
        decade.put("3", "Trente");
        decade.put("4", "Quarante");
        decade.put("5", "Cinqante");
        decade.put("6", "Soixante");
        decade.put("7", "Soixante-Dix");
        decade.put("8", "Quatre-vingt");
        decade.put("9", "Quatre-vingt-Dix");
        hundred = new HashMap<>();
        for (String key : unite.keySet()) {
            String uni = unite.get(key);
            hundred.put(key, uni + " cent");
        }
        decadeParticularity = new HashMap<>();
        decadeParticularity.put("Un", "onze");
        decadeParticularity.put("Deux", "douze");
        decadeParticularity.put("Trois", "treize");
        decadeParticularity.put("Quatre", "quatorze");
        decadeParticularity.put("Cinq", "quinze");
        decadeParticularity.put("Six", "seize");
        decadeParticularity.put("Sept", "Dix-sept");
        decadeParticularity.put("Huit", "Dix-huit");
        decadeParticularity.put("Neuf", "Dix-neuf");
        normalizer = new HashMap<>();
        normalizer.put("-zéro", "");
        normalizer.put("-Zéro", "");
        normalizer.put("Zéro c", "C");
        normalizer.put("Un c", "C");
        normalizer.put("cent-", "cent ");
        normalizer.put("Cent-", "Cent ");
        normalizer.put(" zéro", "");
        normalizer.put("-un", " et un");
        for (String key : decadeParticularity.keySet())
            normalizer.put("Dix-" + key.toLowerCase(), decadeParticularity.get(key));
        priority = new HashMap[] { unite, decade, hundred };
    }

    public static String normalise(String toLetter) {
        String normalized = toLetter;
        for (String key : normalizer.keySet())
            normalized = normalized.replace(key, normalizer.get(key));
        return normalized.intern();
    }
}
