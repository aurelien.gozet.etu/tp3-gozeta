package fr.univlille.iutinfo;

import java.util.HashMap;

public class Numeral {

	protected String number;

	public Numeral(String number) {
		this.number = number;
	}

	public String toLetters() {
		int size = number.length();
		String toLetter = "";
		for (int i = size; i > 0; i--) {
			HashMap<String, String> map = NumeralUtils.priority[size - i];
			String numeral = number.charAt(i - 1) + "";
			toLetter = map.get(numeral) + (size > 1 && !toLetter.equals("") ? "-" + toLetter.toLowerCase() : "");
			toLetter = NumeralUtils.normalise(toLetter);
		}
		return toLetter;
	}

}
