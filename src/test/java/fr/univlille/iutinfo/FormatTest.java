package fr.univlille.iutinfo;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

public class FormatTest {
	
	@Test
    public final void should_return_unite() {
        Numeral numeral = new Numeral("1");
        assertEquals("Un", numeral.toLetters());
        numeral = new Numeral("7");
        assertEquals("Sept", numeral.toLetters());
    }

    @Test
    public final void should_return_first_decade() {
        Numeral numeral = new Numeral("10");
        assertEquals("Dix", numeral.toLetters());
        numeral = new Numeral("18");
        assertEquals("Dix-huit", numeral.toLetters());
    }

    @Test
    public final void should_return_particular_decade() {
        Numeral numeral = new Numeral("20");
        assertEquals("Vingt", numeral.toLetters());
        numeral = new Numeral("21");
        assertEquals("Vingt et un", numeral.toLetters());
    }

    @Test
    public final void should_return_particular_decade_2() {
        Numeral numeral = new Numeral("39");
        assertEquals("Trente-neuf", numeral.toLetters());
        numeral = new Numeral("96");
        assertEquals("Quatre-vingt-seize", numeral.toLetters());
    }
    
    @Test
    public final void should_return_hundred() {
        Numeral numeral = new Numeral("100");
        assertEquals("Cent", numeral.toLetters());
        numeral = new Numeral("200");
        assertEquals("Deux cent", numeral.toLetters());
        numeral = new Numeral("375");
        assertEquals("Trois cent soixante-quinze", numeral.toLetters());
    }

}